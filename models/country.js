const Country = sequelize.define(
    'countries',
    {
      name: {
        type: DataTypes.STRING()
      },
      country_code: {
        type: DataTypes.STRING(),
        allowNull: true
      },
      status: {
        type: DataTypes.ENUM('Active', 'Inactive'),
        allowNull: false,
        defaultValue: 'Active'
      }
    },
    {
      paranoid: true,
      tableName: 'countries',
      timestamps: true,
      underscored: true,
      deletedAt: 'deleted_at',
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  )
  
  module.exports = Country