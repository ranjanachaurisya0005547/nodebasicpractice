const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../config/mysql');

const AgentAgencyMap = sequelize.define(
    'agent_agency_map',
    {
      agent_id: {
        type: DataTypes.INTEGER()
      },
      agency_id: {
          type: DataTypes.INTEGER()
        },
    },
    {
      paranoid: true,
      tableName: 'agent_agency_map',
      timestamps: true,
      underscored: true,
      deletedAt: 'deleted_at',
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  )

  module.exports = AgentAgencyMap