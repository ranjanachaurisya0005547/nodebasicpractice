const express = require('express');
const bodyparser = require('body-parser');
const app = express();
require('./config/mysql');
const User = require('./models/user');
const agentAgencyMap = require('./models/agent_agency_map');


app.use(bodyparser.json());

//User.sync({ alter: true });
// User.sync({ force: true })
// agentAgencyMap.sync({ force: true });
//User.drop();


// app.get('/', function (req, res) {
//     console.log("hello this is awesome !");
// });

app.listen(3000, () => {
    console.log("App will Run On http://localhost:3000");
});